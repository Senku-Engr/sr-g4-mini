import java.util.Scanner;

public class Save {
    boolean saving;

    boolean isSaving(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to save to Record ? [Y/y] or [N/n]: ");
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            saving = false;
        } else if (s.equals("Y") || s.equals("y")) {
            saving = true;
        } else {
            isSaving();
        }
        return saving;
    }

    void saveDB(){
        boolean save = isSaving();
        if (save == true){
            DeleteProduct.deleteAllRecovery();
            System.out.println("------------------------");
            System.out.println("     Record Saved");
            System.out.println("------------------------");
        }else {
            System.out.println("------------------------");
            System.out.println("    Record Canceled");
            System.out.println("------------------------");
        }

    }

}
