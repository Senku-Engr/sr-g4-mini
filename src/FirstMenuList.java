import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

public class FirstMenuList {
    public static void run()
    {
        Table t = new Table(9, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
        t.addCell("*)Display");
        t.addCell(" | W)rite");
        t.addCell(" | R)ead");
        t.addCell(" | U)pdate");
        t.addCell(" | D)elete");
        t.addCell(" | F)irst");
        t.addCell(" | P)revious");
        t.addCell(" | N)ext");
        t.addCell(" | L)ast");
        t.addCell("S)earch");
        t.addCell(" | G)oto");
        t.addCell(" | Se)trow");
        t.addCell(" | Sa)ve");
        t.addCell(" | H)elp");
        t.addCell(" | E)xit");
        System.out.println(t.render());
    }

}
