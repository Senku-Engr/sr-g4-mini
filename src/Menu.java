
import java.util.Scanner;

public class Menu {
    int option;

    void FULLDISPLAY() {
        header();
        recovery();
        fullMenu();
    }

    void fullMenu() {
        list();
        int opt = getOption();
        switch (opt) {
            case 1:
                display();
                fullMenu();
                break;
            case 2:
                write();
                fullMenu();
                break;
            case 3:
                read();
                fullMenu();
                break;
            case 4:
                update();
                fullMenu();
                break;
            case 5:
                delete();
                fullMenu();
                break;
            case 6:
                first();
                fullMenu();
                break;
            case 7:
                previous();
                fullMenu();
                break;
            case 8:
                next();
                fullMenu();
                break;
            case 9:
                last();
                fullMenu();
                break;
            case 10:
                search();
                fullMenu();
                break;
            case 11:
                GOTO();
                fullMenu();
                break;
            case 12:
                setRow();
                fullMenu();
                break;
            case 13:
                save();
                fullMenu();
                break;
            case 14:
                help();
                fullMenu();
                break;
            case 100:
                write10M();
                fullMenu();
                break;
            case 15:
                System.out.println(" Thank you ");
                System.exit(0);
            default:
                fullMenu();
                break;
        }
    }

    int getOption() {
        System.out.print(" Command --> ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (s.equals("*")) {
            option = 1;
        } else if (s.equals("w") || s.equals("W")) {
            option = 2;
        } else if (s.equals("r") || s.equals("R")) {
            option = 3;
        } else if (s.equals("u") || s.equals("U")) {
            option = 4;
        } else if (s.equals("d") || s.equals("D")) {
            option = 5;
        } else if (s.equals("f") || s.equals("F")) {
            option = 6;
        } else if (s.equals("p") || s.equals("P")) {
            option = 7;
        } else if (s.equals("n") || s.equals("N")) {
            option = 8;
        } else if (s.equals("l") || s.equals("L")) {
            option = 9;
        } else if (s.equals("s") || s.equals("S")) {
            option = 10;
        } else if (s.equals("g") || s.equals("G")) {
            option = 11;
        } else if (s.equals("se") || s.equals("Se") || s.equals("sE") || s.equals("SE")) {
            option = 12;
        } else if (s.equals("sa") || s.equals("Sa") || s.equals("sA") || s.equals("SA")) {
            option = 13;
        } else if (s.equals("h") || s.equals("H")) {
            option = 14;
        } else if (s.equals("e") || s.equals("E")) {
            option = 15;
        }
        else if (s.equals("_10M")) {
            option = 100;
        }else
         {
            getOption();
        }
        return option;
    }
    void help(){
        System.out.println("+-----------------------------------------------------------+");
        System.out.println("! 1.    Press   *    : Display all records of product       !");
        System.out.println("! 2.    Press   w    : Add new Product                      !");
        System.out.println("! 3.    Press   r    : read Product information using ID    !");
        System.out.println("! 4.    Press   u    : Update Product using ID              !");
        System.out.println("! 5.    Press   d    : delete Product using ID              !");
        System.out.println("! 6.    Press   f    : Display first page                   !");
        System.out.println("! 7.    Press   p    : Display previous page                !");
        System.out.println("! 8.    Press   n    : Display next page                    !");
        System.out.println("! 9.    Press   l    : Display last page                    !");
        System.out.println("! 10.   Press   s    : Search product by name               !");
        System.out.println("! 11.   Press   g    : Display specific page                !");
        System.out.println("! 12.   Press   se   : Display next page                    !");
        System.out.println("! 13.   Press   sa   : Save to database                     !");
        System.out.println("! 14.   Press   h    : help                                 !");
        System.out.println("! 15.   Press   _10M : Test 10,000,000 records              !");
        System.out.println("! 16.   Press   e    : Exit program                         !");
        System.out.println("+-----------------------------------------------------------+");

    }

    void list() {
        System.out.println();
        FirstMenuList.run();
    }

    void header() {
        Thread t = new Thread(new Thread_header("Please Wait Loading....."));
        t.start();
        try {
            t.join();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    void write10M() {
        Write10M w = new Write10M();
        w.write10M();
    }

    void display() {
        Pagination p = new Pagination();
        p.display();
    }

    void first() {
        Pagination p = new Pagination();
        p.first();
    }

    void last() {
        Pagination p = new Pagination();
        p.last();
    }

    void GOTO() {
        Pagination p = new Pagination();
        p.Goto();
    }

    void next() {
        Pagination p = new Pagination();
        p.next();
    }

    void previous() {
        Pagination p = new Pagination();
        p.previous();
    }

    void save() {
        Save s = new Save();
        s.saveDB();
    }

    void delete() {
        DeleteProduct d = new DeleteProduct();
        d.deleteProduct();
    }

    void update() {
        UpdateProduct u = new UpdateProduct();
        u.innerMenu();
    }

    void search() {
        SearchProduct.searchProduct();
    }

    void setRow() {
        ShowTable.setSetRow();
    }

    void recovery() {
        System.out.println();
        Recovery rec = new Recovery();
        rec.recoverProduct();
    }

    void write() {
        WriteToDB w = new WriteToDB();
        w.getAllData_Write();
    }

    void read() {
        ReadProduct r = new ReadProduct();
        r.read();
    }
}
