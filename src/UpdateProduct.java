

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Scanner;

public class UpdateProduct {
    int id;
    int option;
    Date OGdate;
    boolean isUpdate;
    String name;
    float unit_price;
    Date date;
    int qty;

    boolean isUpdating() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Are you sure you want to Update this record? [Y/y] or [N/n]: ");
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            isUpdate = false;
        } else if (s.equals("Y") || s.equals("y")) {
            isUpdate = true;
        } else {
            isUpdating();
        }
        return isUpdate;
    }

    int getUpdateID(){
        System.out.print("Please input ID of Product : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (Validate.regexNumber(s)==true){
            id = Integer.parseInt(s);
        }else {
            getUpdateID();
        }
        return id;
    }
    void innerMenu(){

        int count =0;
        int id = getUpdateID();
        Connection c = Connect_to_DB.connectDB();
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = c.prepareStatement("SELECT * FROM stock_management WHERE id = ?");
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                 name = rs.getString("name");
                 unit_price = rs.getFloat("unit_price");
                 qty = rs.getInt("qty");
                 date = rs.getDate("import_date");
                 OGdate = date;
                count ++;
                ShowTable.box(id,name,unit_price,qty,date);
            }
            if (count == 0){
                System.out.println("-------------------------------");
                System.out.println("     Item ID : " + id + " not exist");
                System.out.println("-------------------------------");
            }else {
                System.out.println("What do you want to update");
                System.out.println("----------------------------------------------------------------------");
                System.out.println("    1.All   2.Name  3.Quantity  4.Unit Price    5.Back to Menu");
                System.out.println("----------------------------------------------------------------------");
                int opt = getOption();
                WriteToDB w = new WriteToDB();
                switch(opt){
                    case 1 :
                        String updateName = w.getName();
                        float updatePrice = w.getUnit_price();
                        int updateQty = w.getQty();
                        ShowTable.box(id,updateName,updatePrice,updateQty,OGdate);
                        boolean updating = isUpdating();
                        if (updating == true){
                            updateRecord(id,updateName,updatePrice,updateQty,OGdate);
                        }else {
                            System.out.println("-------------------------");
                            System.out.println("         Canceled        ");
                            System.out.println("-------------------------");
                        }
                        break;
                    case 2 :
                        String updateNameOnly = w.getName();
                        ShowTable.box(id,updateNameOnly,unit_price,qty,OGdate);
                        boolean updatingName = isUpdating();
                        if (updatingName == true){
                            updateRecord(id,updateNameOnly,unit_price,qty,OGdate);
                        }else {
                            System.out.println("-------------------------");
                            System.out.println("         Canceled        ");
                            System.out.println("-------------------------");
                        }
                        break;
                    case 3:
                        int updateQtyOnly = w.getQty();
                        ShowTable.box(id,name,unit_price,updateQtyOnly,OGdate);
                        boolean updatingQty = isUpdating();
                        if (updatingQty == true){
                            updateRecord(id,name,unit_price,updateQtyOnly,OGdate);
                        }else {
                            System.out.println("-------------------------");
                            System.out.println("         Canceled        ");
                            System.out.println("-------------------------");
                        }
                        break;
                    case 4:
                        float updatePriceOnly = w.getUnit_price();
                        ShowTable.box(id,name,updatePriceOnly,qty,OGdate);
                        boolean updatingPrice = isUpdating();
                        if (updatingPrice == true){
                            updateRecord(id,name,updatePriceOnly,qty,OGdate);
                        }else {
                            System.out.println("-------------------------");
                            System.out.println("         Canceled        ");
                            System.out.println("-------------------------");
                        }
                    break;
                    case 5 :
                        System.out.println("to menu");
                        break;
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    void updateRecord(int id,String name,float unit_price,int qty,Date date){
        Connection c = Connect_to_DB.connectDB();
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = c.prepareStatement("UPDATE stock_management SET name = ?, unit_price = ?, qty = ?,import_date = ? WHERE id = ?");
            preparedStatement.setString(1,name);
            preparedStatement.setFloat(2,unit_price);
            preparedStatement.setInt(3,qty);
            preparedStatement.setDate(4, (java.sql.Date) date);
            preparedStatement.setInt(5,id);
            preparedStatement.executeUpdate();
            System.out.println("---------------------------");
            System.out.println("   Updated Successfully    ");
            System.out.println("---------------------------");

            preparedStatement.close();
            c.close();

        }catch (Exception e){
            System.out.println(e);
        }

    }
    int getOption(){
        System.out.print("Option (1-5) : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (Validate.regexNumber(s)==true){
            int i = Integer.parseInt(s);
            if(i>=1 && i<=5){
                option = i;
            }else {
                getOption();
            }
        }else {
            getOption();
        }
        return option;
    }
}
