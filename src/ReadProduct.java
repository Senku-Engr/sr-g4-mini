import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Scanner;

public class ReadProduct {
    int id;

    int getId(){
        System.out.print("ID : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if(Validate.regexNumber(s)==true){
            id = Integer.parseInt(s);
        }else {
            getId();
        }
        return id;
    }
    void read(){
        int count = 0;
        int id = getId();
        Connection c  = Connect_to_DB.connectDB();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = c.prepareStatement("SELECT * FROM stock_management WHERE id = ?");
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int idd = rs.getInt("id");
                String name = rs.getString("name");
                float unit_price  = rs.getFloat("unit_price");
                int qty = rs.getInt("qty");
                Date date = rs.getDate("import_date");
                ShowTable.box(id,name,unit_price,qty,date);
                count ++;
            }
            if (count == 0){
                System.out.println("-------------------------------");
                System.out.println("     Item ID : " + id + " not exist");
                System.out.println("-------------------------------");
            }

            preparedStatement.close();
            c.close();
            rs.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
