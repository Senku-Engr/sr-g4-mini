import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.util.Date;
import java.util.Scanner;
// please add the library in lib folder as library to render table
// please use jdk 13.0.2 link to install in the notepad file

public class ShowTable {

    int count = 0;
    static int setRow = 3;
    boolean headtable = false;
    Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);

    public static void setSetRow(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter Row for Display : ");
        String s = sc.nextLine();
        if (Validate.regexNumber(s) == false){
            setSetRow();
        }else{
            setRow = Integer.parseInt(s);
        }
    }

    public static void box(int id, String name, float unit_price, int qty, Date date){
        Table t = new Table(2, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND
        );
        t.addCell("ID");
        t.addCell(" : " + id);
        t.addCell("Name");
        t.addCell(" : " + name);
        t.addCell("Unit price");
        t.addCell(" : " + unit_price);
        t.addCell("Qty");
        t.addCell(" : " + qty);
        t.addCell("Imported Date");
        t.addCell(" : " + date);
        System.out.println(t.render());
    }

    public void table(int id, String name, float unit_price, int qty, Date date) {

        if (headtable == false) {
            t.addCell("ID");
            t.addCell("Name");
            t.addCell("Unit_price");
            t.addCell("qty");
            t.addCell("imported-Date");
            headtable = true;
        }
        if (count < setRow){
            count++;
            t.addCell(id + "");
            t.addCell(name + "");
            t.addCell(unit_price + "");
            t.addCell(qty + "");
            t.addCell(date + "");
        }
    }
    // ********** must use render table to render table after add all row to table
    public void renderTable(){
        System.out.println(t.render());
    }
}



// how to use box
//ShowTable.box(idd, name, unit_price, qty, datee);



/*
// how to use table function for pagination
// the way to create method is similar to this
// create another method in other class or the same class the method should have one parameter for what page you want to show
// then you should be ok working with the table method i have create
// display your pagination page and total record after render the table
// the function below is just an example
void mytable(){
        try {
            ShowTable t1 = new ShowTable();
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stock_management");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                float unit_price = rs.getFloat("unit_price");
                int qty = rs.getInt("qty");
                Date datee = rs.getDate("import_date");
                t1.table(id, name, unit_price, qty, datee);
            }
            t1.renderTable();    // must use this method to render table after add all row
            // your code display pagination and total record here
        }catch (Exception e){
            System.out.println(e);
        }
    }
 */
