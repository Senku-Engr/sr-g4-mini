import java.sql.Connection;
import java.sql.PreparedStatement;

import java.util.Scanner;

public class Write10M {
    boolean is10m;

    boolean isWriting() {
        System.out.print("Are you sure you want to write 10,000,000 Records, old record will be overwrite ? [Y/y] or [N/n]: ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            is10m = false;
        } else if (s.equals("Y") || s.equals("y")) {
            is10m = true;
        } else {
            isWriting();
        }
        return is10m;
    }

    void write10M() {

        boolean a = isWriting();
        if (a == true) {
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            Connection c = Connect_to_DB.connectDB();
            PreparedStatement preparedStatement = null;
            try {
                DeleteProduct.deleteAllstock();
                DeleteProduct.deleteAllRecovery();
                for (int i = 1; i <= 10000000; i++) {
                    preparedStatement = c.prepareStatement("INSERT INTO stock_management VALUES (?,?,?,?,?)");
                    preparedStatement.setInt(1, i);
                    preparedStatement.setString(2, "Ara Ara");
                    preparedStatement.setFloat(3, 0);
                    preparedStatement.setInt(4, 0);
                    preparedStatement.setDate(5, date);
                    preparedStatement.executeUpdate();
                }
                preparedStatement.close();
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }



        } else {
            System.out.println("-------------------------");
            System.out.println("         Canceled        ");
            System.out.println("-------------------------");
        }
    }
}


