import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;

public class Recovery {
    boolean isrecover;

    boolean isRecover() {
        Scanner sc = new Scanner(System.in);
        System.out.print("You missed to save record do you want to save it? [Y/y] or [N/n]: ");
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            isrecover = true;
        } else if (s.equals("Y") || s.equals("y")) {
            isrecover = false;
        } else {
            isRecover();
        }
        return isrecover;
    }

    // main recovery method
    void recoverProduct() {
        int count = 0;
        try {
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM recovery");
            while (rs.next()) {
                count++;
            }
            rs.close();
            if (count != 0) {
                boolean recovering = isRecover();
                if (recovering == true) {
                    ResultSet rss = stmt.executeQuery("SELECT * FROM recovery");
                    while (rss.next()) {
                        int idd = rss.getInt("id");
                        String name = rss.getString("name");
                        float unit_price = rss.getFloat("unit_price");
                        int qty = rss.getInt("qty");
                        Date datee = rss.getDate("import_date");

                        PreparedStatement preparedStatement = c.prepareStatement("INSERT INTO stock_management VALUES(?,?,?,?,?)");
                        preparedStatement.setInt(1, idd); //increase id, by yourself
                        preparedStatement.setString(2, name);
                        preparedStatement.setFloat(3, unit_price);
                        preparedStatement.setInt(4, qty);
                        preparedStatement.setDate(5, (java.sql.Date) datee);
                        preparedStatement.executeUpdate();

                        DeleteProduct.deleteFromRecovery(idd);
                    }
                    rss.close();
                }
            }

            c.close();
            rs.close();
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
