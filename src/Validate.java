import java.util.regex.Pattern;

public class Validate {
    static boolean regexNumber(String s){
        return Pattern.matches("\\d+", s);
    }
    static boolean regexName(String s){
        return Pattern.matches("\\D+", s);
    }
    static boolean regexFloat(String s) {
        return Pattern.matches("[0-9]+(\\.[0-9]+)?([0-9]+)?", s);
    }
}
