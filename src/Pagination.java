
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;

public class Pagination {
    int totalRecord;
   static int pageNumber = 1;

   void display(){
       pagination(pageNumber);
   }

    void first(){
        pageNumber = 1;
        pagination(pageNumber);
    }
    void last(){
        int tRecord = getAllRecord();
        int tpage = (int) Math.ceil(tRecord / ShowTable.setRow);
        if (tRecord % ShowTable.setRow > 0) {
            tpage = tpage + 1;
        }
        pageNumber = tpage;
        pagination(pageNumber);
    }
    void Goto() {
        int tRecord = getAllRecord();
        int tpage = (int) Math.ceil(tRecord / ShowTable.setRow);
        if (tRecord % ShowTable.setRow > 0) {
            tpage = tpage + 1;
        }
        System.out.print("Goto page (1-" + tpage + ") :");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (Validate.regexNumber(s) == true) {
            int a = Integer.parseInt(s);
            if (a >= 1 && a <= tpage) {
                pagination(a);
            } else {
                Goto();
            }

        } else {
            Goto();
        }
    }

    void next() {
        int tRecord = getAllRecord();
        int tpage = (int) Math.ceil(tRecord / ShowTable.setRow);
        if (tRecord % ShowTable.setRow > 0) {
            tpage = tpage + 1;
        }
        if (pageNumber == tpage) {
            pageNumber = 1;
        } else {
            pageNumber = pageNumber + 1;
        }
        pagination(pageNumber);
    }

    void previous() {
        int tRecord = getAllRecord();
        int tpage = (int) Math.ceil(tRecord / ShowTable.setRow);
        if (tRecord % ShowTable.setRow > 0) {
            tpage = tpage + 1;
        }
        if (pageNumber == 1) {
            pageNumber = tpage;
        } else {
            pageNumber = pageNumber - 1;
        }
        pagination(pageNumber);
    }

    int getAllRecord() {
        Connection c = Connect_to_DB.connectDB();
        Statement stmt = null;
        totalRecord = 0;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stock_management");
            while (rs.next()) {
                totalRecord++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return totalRecord;
    }

    void pagination(int pageNum) {
        int tRecord = getAllRecord();
        int tpage = (int) Math.ceil(tRecord / ShowTable.setRow);
        if (tRecord % ShowTable.setRow > 0) {
            tpage = tpage + 1;
        }
        int end = pageNum * ShowTable.setRow;
        int start = end - ShowTable.setRow;
        int count = 0;

        try {
            ShowTable t1 = new ShowTable();
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stock_management ORDER BY id ASC ");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                float unit_price = rs.getFloat("unit_price");
                Date date = rs.getDate("import_date");
                int qty = rs.getInt("qty");
                if (count >= start) {
                    t1.table(id, name, unit_price, qty, date);
                }
                count++;
            }
            t1.renderTable();
            System.out.println("--------------------------------------------------");
            System.out.println(" Page : " + pageNum + " of " + tpage + "                   total Record : " + tRecord);
            System.out.println("--------------------------------------------------");

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
