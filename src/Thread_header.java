class Thread_header implements Runnable {
    String msg;
    Thread t;

    Thread_header(String str) {
        t = new Thread(this);
        msg = str;
    }

    public void run() {
        PrintMsg(msg);
    }

    synchronized void PrintMsg(String msg) {
        System.out.println("                             Wellcome to");
        System.out.println("                          Stock Mangement");
        System.out.println
                (" _____ _               ______                 _____                         ___ \n" +
                        "/  ___(_)              | ___ \\               |  __ \\                       /   |\n" +
                        "\\ `--. _  ___ _ __ ___ | |_/ /___  __ _ _ __ | |  \\/_ __ ___  _   _ _ __  / /| |\n" +
                        " `--. \\ |/ _ \\ '_ ` _ \\|    // _ \\/ _` | '_ \\| | __| '__/ _ \\| | | | '_ \\/ /_| |\n" +
                        "/\\__/ / |  __/ | | | | | |\\ \\  __/ (_| | |_) | |_\\ \\ | | (_) | |_| | |_) \\___  |\n" +
                        "\\____/|_|\\___|_| |_| |_\\_| \\_\\___|\\__,_| .__/ \\____/_|  \\___/ \\__,_| .__/    |_/\n" +
                        "                                       | |                         | |          \n" +
                        "                                       |_|                         |_| ");

        for (int i = 0; i < msg.length(); i++) {

            System.out.print(msg.charAt(i));
            try {
                Thread.sleep(300);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
