


import java.sql.*;
import java.util.Date;
import java.util.Scanner;

public class WriteToDB {
    boolean isWrite;

    int maxID = 1;
    String writeName;
    float unit_price;
    int qty;

    int getQty(){
        System.out.print("Qty : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        if(Validate.regexNumber(s) == true){
            qty = Integer.parseInt(s);
        }else {
            getQty();
        }
        return qty;
    }

    String getName (){
        System.out.print("Name : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();

        if(Validate.regexName(s) == true){
            writeName = s;
        }else {
            getName();
        }
        return writeName;
    }

    float getUnit_price(){
        System.out.print("Unit_price : ");
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        if (Validate.regexFloat(s) == true){
            unit_price = Float.parseFloat(s);
        }
        else {
            getUnit_price();
        }
        return unit_price;
    }

    int getAUTOid() {
        try {
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM stock_management");
            while (rs.next()) {
                int id = rs.getInt("id");
                if (id > maxID) {
                    maxID = id;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return maxID;
    }

    boolean isWriting(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Are you sure you want to write this record ? [Y/y] or [N/n]: ");
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            isWrite = false;
        } else if (s.equals("Y") || s.equals("y")) {
            isWrite = true;
        } else { isWriting();}
        return isWrite;
    }

    void getAllData_Write(){
        String name = getName();
        float unit_price = getUnit_price();
        int qty = getQty();
        int id = getAUTOid() + 1;
        long millis=System.currentTimeMillis();
        java.sql.Date date=new java.sql.Date(millis);
        // write
        boolean iswrite = isWriting();
        ShowTable.box(id,name,unit_price,qty,date);
        if (iswrite == true){
            WriteDB(id,name,unit_price,qty,date);
            System.out.println("-------------------------");
            System.out.println("      Record Wrote       ");
            System.out.println("-------------------------");
        }else {
            System.out.println("-------------------------");
            System.out.println("         Canceled        ");
            System.out.println("-------------------------");
        }
    }
    void WriteDB(int id, String name, Float unit_price, int qty, Date date){
        Connection c = Connect_to_DB.connectDB();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = c.prepareStatement("INSERT INTO stock_management VALUES (?,?,?,?,?)");
            preparedStatement.setInt(1,id);
            preparedStatement.setString(2,name);
            preparedStatement.setFloat(3,unit_price);
            preparedStatement.setInt(4,qty);
            preparedStatement.setDate(5, (java.sql.Date) date);
            preparedStatement.executeUpdate();
            c.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
